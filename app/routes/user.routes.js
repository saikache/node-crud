import express from 'express';
const router = express.Router();
import { findAll, findOne, create, update, deleteUser } from '../controllers/user.controller.js'

  router.get('/users', findAll);
  router.get('/users/:userId', findOne);
  router.put('/users/:userId', update);
  router.delete('/users/:userId', deleteUser);

export default router;
