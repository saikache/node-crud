/**
 * @swagger
 *
 * /login:
 *   post:
 *     description: Login to the application
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: body
 *         name: email
 *         schema:
 *           type: object
 *           required:
 *              - email
 *              - password
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string 
 *     responses:
 *       200:
 *         description: login
 *         schema:
 *           type: object
 *           properties:
 *             accessToken:
 *               type: string
 *               description: JWT token to pass consecutive APIs in Headers with key token
 *       404:
 *         description: Wrong password
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *               description: Password does not matched
 *
 

 * /users:
 *   get:
 *     description: To display all users
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: header
 *         name: token
 *         description: Bearer token got from login responses
 *         required: true
 *         type: string
 *     responses:
 *       200:
 *         description: login
 *         schema:
 *           type: object
 *           properties:
 *             accessToken:
 *               type: string
 *               description: JWT token to pass consecutive APIs in Headers with key token
 *       404:
 *         description: Wrong password
 *         schema:
 *           type: object
 *           properties:
 *             message:
 *               type: string
 *               description: Password does not matched


 * /users/{userId}:
 *   get:
 *     description: Display a single user
 *

 *   put:
 *     description: Update a single user
 *     parameters:
*       - in: header
 *         name: token
 *         description: Bearer token got from login responses
 *         required: true
 *         type: string
 *       - in: body
 *         name: name
 *         schema:
 *           type: object
 *           required:
 *              - name
 *           properties:
 *             name:
 *               type: string

 *   delete:
 *     description: To delete single user from DB
 *
 *



 */
