import User from '../models/user.model.js'
import bcrypt from 'bcrypt';

// Retrieve and return all notes from the database.
const findAll = (req, res) => {
  User.find()
  .then(users => {
    res.send(users);
  }).catch(err => {
    res.status(500).send({
      message: err.message || "Some error occurred while retrieving users."
    });
  });
};

// Find a single user with a userId
const findOne = (req, res) => {
  User.findById(req.params.userId)
  .then(user => {
    if(!user) {
      return res.status(404).send({
        message: "user not found with id " + req.params.userId
      });
    }
    res.send(user);
  }).catch(err => {
    if(err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "user not found with id " + req.params.userId
      });
    }
    return res.status(500).send({
      message: "Error retrieving user with id " + req.params.noteId
    });
  });
};

const findByEmail = (req, res) => {
  User.findOne({email: req.params.email})
  .then(user => {
    if(!user) {
      return res.status(404).send({
        message: "user not found with id " + req.params.userId
      });
    }
    res.send(user);
  }).catch(err => {
    if(err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "user not found with id " + req.params.userId
      });
    }
    return res.status(500).send({
      message: "Error retrieving user with id " + req.params.noteId
    });
  });
};

const getPasswordSalt = (password, callBack) => {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(password, salt, function(err, hash) {
      if (err) {
        callBack(err)
      } else {
        callBack('', hash)
      }
    });
  });
}

// Create and Save a new User
const create = (req, res) => {
  // Validate request
  if(!req.body.password || !req.body.name || !req.body.email ) {
    return res.status(400).send({
      message: "Email / Password / name must be present"
    });
  }

  getPasswordSalt(req.body.password, (error, pwtSalt) => {
    if (error) {
      res.status(500).send({
        message: error.message || "Some error occurred on encripting password salt."
      });
    } else { // success
      const user = new User({ name: req.body.name,  email: req.body.email, password: pwtSalt });
      user.save()
      .then(data => {
        res.send(data);
      }).catch(err => {
        if (err.name == 'ValidationError') {
          let errors = [];
          Object.keys(err.errors).forEach(single_attr => {
            errors.push(err.errors[single_attr].message);
          });
          res.status(500).send({
            messages: errors
          });
        } else {
          res.status(500).send({
            message: err.message || "Some error occurred while creating the User."
          });
        }
      });
    }
  })
};

// Update a user identified by the userId
const update = (req, res) => {
    // Validate Request
    if(!req.body.name) {
      return res.status(400).send({
        message: "User name can not be empty"
      });
    }

    // Find user and update
    User.findOneAndUpdate(req.params.userId, {
      name: req.body.name
    }, { runValidators: true, context: 'query' })
    .then(user => {
      if(!user) {
        return res.status(404).send({
          message: "user not found with id " + req.params.userId
        });
      }
      res.send(user);
    }).catch(err => {
      if (err.name == 'ValidationError') {
        let errors = [];
        Object.keys(err.errors).forEach(single_attr => { 
          errors.push(err.errors[single_attr].message);
        });
        res.status(500).send({
          messages: errors
        });
      } else {
        res.status(500).send({
          message: err.message || "Some error occurred while creating the User."
        });
      }
    });
  };

// Delete user
const deleteUser = (req, res) => {
  User.findByIdAndRemove(req.params.userId)
  .then(user => {
    if(!user) {
      return res.status(404).send({
        message: "User not found with id " + req.params.userId
      });
    }
    res.send({message: "User deleted successfully!"});
  }).catch(err => {
    console.log(err)
    return res.status(404).send({
      message: "user not found with id " + req.params.userId
    });
    return res.status(500).send({
      message: "Could not delete user with id " + req.params.userId
    });
  });
};

export { findAll, findOne, create, update, deleteUser, findByEmail }
