// User Schema
import mongoose from 'mongoose'
import uniqueValidator from 'mongoose-unique-validator'
const { Schema } = mongoose

var UserSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Name must be present']
  },
  email: {
    type: String,
    required: [true, 'Email is present'],
    unique: true
  },
  password: {
    type: String,
    required: [true, 'Password must be present'],
  }
},
{
  timestamps: true
});

UserSchema.plugin(uniqueValidator, {message: 'Error, {PATH} already has been taken'});
const User = mongoose.model('User', UserSchema);
export default User
