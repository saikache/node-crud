import njwt from 'njwt';
import bcrypt from 'bcrypt';
import dbConfig from '../../config/db-config.js'
import User from '../../app/models/user.model.js'

// Move this secret token to ENV variable
const encodeToken = (tokenData) => njwt.create(tokenData, "cb475dea80d39c54001e14a8f624cd8a83784289a67576307").compact()
const decodeToken = (token) => njwt.verify(token, 'cb475dea80d39c54001e14a8f624cd8a83784289a67576307').body

const deCodePassword = (password, pwdSalt, callback) => {
  bcrypt.compare(password, pwdSalt, function(err, result) {
    if (err) {
      callback(err)
    } else {
      callback('', result)
    }
  })
}

const jwtLogin = (req, res) => {
  const { email, password } = req.body;
  console.log(email, password, '33333333333333')
  User.findOne({email: email})
  .then(user => {
    if(!user) {
      return res.status(401).send({
        message: "User not found"
      });
    }
    deCodePassword(password, user.password, function(error, result) {
      if (error) {
        return res.status(500).send({
          message: error
        });
      } else {
        if (result == true) {
          const accessToken = encodeToken({ userId: user.id });
          return res.json({ accessToken });
        } else { // not matched
          return res.status(404).send({
            message: 'Password does not matched'
          });
        }
      }
    })
  }).catch(err => {
    return res.status(404).send({
      message: "Error" + `${err}`
    });
  });
}

const jwtAuthenticationMiddleware = (req, res, next) => {
  const token = req.header('token');
  if (!token)
    return res.status(401).send({ message: 'Auth token missing' });
  try {
    const decoded = decodeToken(token);
    const { userId } = decoded;
    console.log('decoded', decoded);
    console.log('userId', userId);
    User.findOne({_id: userId})
    .then(user => {
      if(!user) {
        return res.status(404).send({
          message: "User not found"
        });
      } else {
        req.userId = userId; // is it required ?
        setTimeout(() => next(), 1000)
      }
    }).catch(err => {
      if(err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "user not found ***************" //+ req.params.userId
        });
      }
      return res.status(500).send({
        message: "Error retrieving user with id " + err
      });
    });
  } catch (e) {
    return res.json({error: e.name, full_message: e})
  }
  next();
};

export { jwtLogin, jwtAuthenticationMiddleware }
