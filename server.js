import express from 'express';
import dbConfig from './config/db-config.js'
import bodyParser from 'body-parser'
import mongoose from 'mongoose'
import { jwtAuthenticationMiddleware, jwtLogin } from './app/auth/jwt-authentication.js';
import user_router from './app/routes/user.routes.js'
import { create } from './app/controllers/user.controller.js'
import swaggerUi from 'swagger-ui-express';
import swaggerJsDoc from 'swagger-jsdoc';
const app = express();
app.use(bodyParser.json())

const swaggerOption = {
	swaggerDefinition: {
		info: {
			title: 'Techwave - Node',
			description: 'APIs documentation',
			servers: ['http://localhost:3001']
		}
	},
	apis: ['./app/routes/swagger.js']
}
const swaggerDocs = swaggerJsDoc(swaggerOption);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs))

// No authentication
app.post('/login', jwtLogin);
app.post('/users', create);
app.get('/', (req, res) => {
  res.json({"message": "API documentation is available", link: "/api-docs"});
});

app.use(jwtAuthenticationMiddleware);
app.use(user_router)

// Connecting to database
mongoose.connect(dbConfig.url, {
  useNewUrlParser: true,
  useUnifiedTopology: true
}).then(() => {
  console.log("Successfully connected to the database");    
}).catch(err => {
  console.log('Could not connect to the database. Exiting now...', err);
  process.exit();
});

app.listen(5000, () => {
  console.log("Server is listening on port 5000");
});
