get all users
Get /users
---------------------

display user
get /users/:userId
---------------------

create user
POST /users

body params
{"name": "Sai", "content": "Sagar"}
---------------------

update user
PUT  /users/:userId

body params
{"name": "Sai", "content": "Sagar"}
---------------------

delete user
DELETE /users/:userId

// "type": "module"

eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIxIiwianRpIjoiMDEwMjA5MDEtYTFmYi00MDlkLWIwNzAtYzYyNDgxMDZkZTlmIiwiaWF0IjoxNjAwMjY1OTU2LCJleHAiOjE2MDAyNjk1NTZ9.GiKjvqhbZFjfx0zHmp-tJbdHzLLim-TRcpcfAsWaAjA


TO-Dos

* Password are saving as is, should be stored as password-salt - Done

* Change njwt scret-key as ENV variable
 